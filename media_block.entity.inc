<?php
/**
 * @file media_block.entity.inc
 */
 
/**
 * Entity class for the MediaBlock
 */
class MediaBlock extends Entity {
  public function __construct($values = array()) {
    parent::__construct($values, 'media_block');
  }
  
  protected function defaultLabel() {
    return $this->admin_title;
  }

  protected function defaultUri() {
    return array('path' => 'admin/structure/block/media_block/' . $this->name);
  }
  
  public function view($view_mode = 'default', $langcode = NULL, $page = NULL) {
    return entity_get_controller($this->entityType)->view(array($this), $view_mode, $langcode, $page);
  }
}
 
/**
 * Controller for the MediaBlockEntity
 */
class MediaBlockController extends EntityAPIControllerExportable {
  /**
   * We want to only render the file in the media_block_media field
   *
   * @override EntityAPIContollerExportable::view
   */
  public function view($entities, $view_mode = 'default', $langcode = NULL, $page = NULL) {
    $view = parent::view($entities, $view_mode, $langcode, $page);

    // Override the view mode.
    foreach ($view[$this->entityType] as $name => $content) {
      if (isset($content['media_block_media'])) {
        $field = field_info_field('media_block_media');
        $instance = field_info_instance('media_block', 'media_block_media', 'media_block');
        $items = field_get_items('media_block', $content['#entity'], 'media_block_media');
        $display = field_get_display($instance, $view_mode, $content['#entity']);
        
        //override the file_view_mode
        $display['settings']['file_view_mode'] = $content['#entity']->view_mode;
        
        $content['media_block_media'] = reset(field_default_view('media_block', $content['#entity'], $field, $instance, LANGUAGE_NONE, $items, $display));
        
        // Check the type of the first item - If its an image we find if it has a resource and link to
        // that unless a link is already set in the ui.
        // @todo: check the file is a resource
        // @todo: save this on media_block save? 
        $item = reset($items);
        if ($item['type'] == 'image') {
          $file = file_load($item['fid']);
          $usage = file_usage_list($file);
          if (isset($usage['file']['node'])) {
            $ids = $usage['file']['node'];
            $ids = array_keys($ids);
            $nodes = node_load_multiple($ids);
            $node = reset($nodes);
            if (!$content['#entity']->link) {
              $content['#entity']->link = TRUE;
              $content['#entity']->link_path = reset(node_uri($node));
            }
            
            if (!$content['#entity']->use_overlay_text) {
              $content['#entity']->use_overlay_text = TRUE;
              $content['#entity']->overlay_text = $node->title;
            }
          }
        }

        $view[$this->entityType][$name] = $content;
      }
    }    
    return $view;
  }
  
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $this->load($ids);
    
    foreach ($entities as $media_block) {
      db_delete('block')
        ->condition('module', 'media_block')
        ->condition('delta', $media_block->name)
        ->execute();
      db_delete('block_role')
        ->condition('module', 'media_block')
        ->condition('delta', $media_block->name)
        ->execute();
    }
  
    parent::delete($ids, $transaction);    
  }  
}