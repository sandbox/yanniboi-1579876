<?php

/**
 * @file
 * A basic template for media_block entities
 *
 */

$link = $media_block->link;
$link_path = $media_block->link_path;
if (!$link_path) {
  $link = FALSE;
}

$overlay = $media_block->use_overlay_text;
$overlay_text = check_plain($media_block->overlay_text);
if (!$overlay_text) {
  $overlay = FALSE;
}

?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ($link): ?><a href="<?php print url($link_path); ?>"<?php if (url_is_external($link_path)) print ' target="_blank"'; ?>><?php endif; ?>
  <?php print render($content); ?>
  <?php if ($overlay) : ?>
  <h5 class="overlay"><?php print $overlay_text; ?></h5>
  <?php endif; ?>
  <?php if ($link): ?></a><?php endif; ?>
</div>
