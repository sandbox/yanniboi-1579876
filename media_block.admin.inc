<?php
/**
 * @file media_block.admin.inc
 */
 
/**
 * Form to create new media blocks
 */
function media_block_add_form($form, &$form_state) {
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Administration Title'),
    '#size' => 60,
    '#required' => TRUE,
  );
  
  $form['name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'exists' => 'media_block_load',
      'source' => array('admin_title'),
      'label' => t('Machine Name'),
    ),
    '#description' => t('The machine readable name for this media block. It must only contain lowercase letters, numbers and underscores.'),
    '#title' => t('Machine Name'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  
  return $form;    
}

function media_block_add_form_validate($form, &$form_state) {

}

function media_block_add_form_submit($form, &$form_state) {
  $media_block = media_block_create();
  $media_block->admin_title = $form_state['values']['admin_title'];
  $media_block->name = $form_state['values']['name'];
  $media_block->view_mode = 'default';
  
  $media_block->save();
  
  $form_state['redirect'] = 'admin/structure/block/manage/media_block/' . $media_block->name . '/configure';
}

/**
 * Form to delete media blocks
 */
function media_block_delete_form($form, &$form_state, $media_block) {
  $form_state['media_block'] = $media_block;
  return confirm_form($form, 
                      t('Are you sure you want to delete the media block %title?', 
                      array('%title' => $media_block->admin_title)), 
                      'admin/structure/block', '', t('Delete'), t('Cancel'));
}

function media_block_delete_form_submit($form, &$form_state) {
  $media_block = $form_state['media_block'];
  $media_block->delete();
  drupal_set_message(t('The media block %name has been removed.', array('%name' => $media_block->name)));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}